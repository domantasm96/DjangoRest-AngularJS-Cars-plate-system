# Cars Plate System

 ### Implemented tasks
 - [X] CRUD operations.
 - [X] Displaying plates and owners name.
 - [X] Django admin view
 - [X] Input valid validations

### Tools
This project is written using Django and Django REST frameworks as backend and for fronted - AngularJS.
Input validations in backend implemented by describing models limit; in frontend - using HTML pattern attributes and regular expression.

Available operations:

1. Display all registered plates
2. Display all registered owners
3. Add, edit, delete plates.
4. Add new owner
5. Different plates for general and electric cars:
	- General cars plate format: XXXYYY - where X letter and Y number
	- Electric cars plate format: EVYYYY - 'EV'-string and Y number
 
### How to launch the project:

1. Activate Virtual environment:
	- __source venv/bin/activate__
2. Install necessary packages:
	- __pip3 install -r requirements.txt__
3. Runserver:
	- __python3 manage.py runserver__

### Technical info about project:

1. URL's:
	- http://127.0.0.1:8000/ : Main page.
	- http://127.0.0.1:8000/api/plates/ : REST API plates page
	- http://127.0.0.1:8000/api/owners/ : REST API owners page
	- http://127.0.0.1:8000/admin/ : Page Admin page
2. Credentials for admin user:
	- Username: admin
	- Password: super1234
