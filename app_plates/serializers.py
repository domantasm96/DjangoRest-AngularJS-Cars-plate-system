from rest_framework import serializers
from .models import *

class OwnerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Owner
        fields = '__all__'


class PlateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Plate
        fields = '__all__'
