from django.db import models
from django.core.validators import RegexValidator

CAR_TYPES = (
    ('General usage', 'GU'),
    ('Electric car', 'EC'),
)

class Owner(models.Model):
    firstname = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)

    def __str__(selfs):
        return "{} {}".format(selfs.firstname, selfs.surname)

class Plate(models.Model):
    owner = models.ForeignKey(Owner, on_delete=models.CASCADE, blank=True, null=True)
    car_type = models.CharField(choices=CAR_TYPES, max_length=20, default='General usage')
    plate_numbers = models.CharField(validators=[RegexValidator(regex='^[A-Za-z]{3}[0-9]{3}$|^EV[0-9]{4}$', message='XXXYYY where X', code='nomatch')], max_length=6)

    def __str__(selfs):
        return "{} {} {}".format(selfs.owner, selfs.car_type, selfs.plate_numbers)

