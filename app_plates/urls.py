from django.conf.urls import url

from . import views

app_name = 'app_plates'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    # url(r'^register/$', views.register, name='register'),
    # url(r'^login/$', views.login_user, name='login'),
    # url(r'^logout/$', views.logout_user, name='logout'),
    # url(r'^profile/$', views.profile, name='profile'),
    # # url(r'^(?P<profile_id>[0-9]+)/$', views.otherProfile, name='otherProfile'),
    # url(r'^profile/(?P<profile_name>[\w\-]+)/$', views.otherProfile, name='otherProfile'),
    # url(r'^upload/$', views.upload_song, name='upload_song'),
    # url(r'^delete/(?P<song_id>[\w\-]+)/$', views.delete_song, name='delete_song'),
    # url(r'^playlist/$', views.playlist, name='playlist'),
    # url(r'^addSong/(?P<song_id>[\w\-]+)/$', views.addSong, name='addSong'),
    # url(r'^likeSong/(?P<song_id>[\w\-]+)/$', views.likeSong, name='likeSong'),
]