from django.apps import AppConfig


class AppPlatesConfig(AppConfig):
    name = 'app_plates'
