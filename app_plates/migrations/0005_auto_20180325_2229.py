# Generated by Django 2.0.2 on 2018-03-25 22:29

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_plates', '0004_auto_20180325_2227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plate',
            name='plate_numbers',
            field=models.CharField(max_length=6, validators=[django.core.validators.RegexValidator(code='nomatch', message='XXXYYY where X', regex='^[A-Za-z]{3}[0-9]{3}$|^EV[0-9]{4}$')]),
        ),
    ]
