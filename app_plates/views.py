from django.shortcuts import render
from .models import *
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404
from .serializers import *


def index(request):
    return render(request, 'index.html', {'plates': Plate.objects.all(), 'owners': Owner.objects.all()})

class OwnerList(APIView):

    def get(self, request):
        owners = Owner.objects.all()
        serializer = OwnerSerializer(owners, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = OwnerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PlateList(APIView):

    def get(self, request):
        plates = Plate.objects.all()
        serializer = PlateSerializer(plates, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = PlateSerializer(data=request.data)
        print(serializer)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PlateViewList(APIView):

    def get(self, request, pk):
        plate = get_object_or_404(Plate, pk=pk)
        serializers = PlateSerializer(plate)
        return Response(serializers.data)

    def put(self, request, pk):
        plate = get_object_or_404(Plate, pk=pk)
        plate.owner.id = pk
        serializer = PlateSerializer(plate, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        plate = get_object_or_404(Plate, pk=pk)
        plate.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
