$(document).ready(function () {
    $("#type").change(function () {
        var val = $(this).val();
        if(val === 'object:3'){
             $("#number").attr('pattern', '^[A-Za-z]{3}[0-9]{3}');
        }
        if(val === 'object:4'){
             $("#number").attr('pattern', '^EV[0-9]{4}');
        }
        })
    });

var app = angular.module('plates_system', []).config(function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});
app.controller('PlatesSystemController', function($scope,$http) {
    $scope.variants = [];
    $scope.variantsTypes = [{'type': 'General usage'}, {'type': 'Electric car'}];
    $http.get('/api/owners/').then(function (response) {
        for (var i = 0; i < response.data.length; i++) {
            var info = {};
            info.id = response.data[i].id;
            info.name = response.data[i].firstname + ' '+response.data[i].surname;
            $scope.variants.push(info);
        }
    });
    $http.get('/api/plates/').then(function (response) {
        $scope.update_owner = {};
        $scope.update_type = {};
        $scope.update_plate = {};
        $scope.platesList = [];
        for (var i = 0; i < response.data.length; i++) {
            var plates = {};
             for (var j = 0; j < $scope.variants.length; j++){
                 if(response.data[i].owner ===  $scope.variants[j].id){
                     plates.owner = $scope.variants[j].name;
                 }
             }
            plates.car_type = response.data[i].car_type;
            plates.numbers = response.data[i].plate_numbers;
            plates.id = response.data[i].id;
            $scope.platesList.push(plates);
        }
    });

    $scope.remove = function(id, index) {
        $http.delete('/api/plates/' + id + '');
        $scope.platesList.splice(index, 1);
    };

    $scope.editSaveData = function(id, plate_id) {
        var data = {owner: $scope.update_owner[id].id, car_type: $scope.update_type[id].type, plate_numbers: $scope.update_plate[id]};
        $http.put('/api/plates/'+ plate_id + '/', data);
        $scope.platesList[id-1].owner = $scope.update_owner[id].name;
        $scope.platesList[id-1].car_type = $scope.update_type[id].type;
        $scope.platesList[id-1].numbers = $scope.update_plate[id];
    };


    $scope.saveData = function() {
        var data = {owner: $scope.selectedOwner.id, car_type: $scope.plate_type.type, plate_numbers: $scope.plate_number};
        $http.post('/api/plates/', data);
    };


    $scope.plateAdd = function() {
        $scope.platesList.push({owner: $scope.selectedOwner.name, car_type: $scope.plate_type.type, numbers: $scope.plate_number});
        $scope.selectedOwner = '';
        $scope.car_type= '';
        $scope.plate_number= '';
    };
    $scope.saveOwner = function () {
        var data = {firstname: $scope.owner_firstname, surname: $scope.owner_surname};
        $http.post('/api/owners/', data);
    };
     $scope.addOwner = function() {
        $scope.variants.push({owner: $scope.owner_firstname.name, surname: $scope.owner_surname});
        $scope.owner_firstname = '';
        $scope.owner_surname = '';
    };
});
